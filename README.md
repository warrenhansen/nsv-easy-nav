# Navigation Wrapper

> A dead simple wrapper for the $navigator in NativeScript-Vue.

## Usage

``` bash
# Install dependencies
npm install

# Build for production
tns build <platform> --bundle

# Build, watch for changes and debug the application
tns debug <platform> --bundle

# Build, watch for changes and run the application
tns run <platform> --bundle
```

## Config

```javascript
let router = new Router({
  routes: [
    { 
      name: 'home',
      component: Master
    },
    { 
      name: 'detail',
      component: Detail
    },
    { 
      name: 'drawer',
      component: Drawer
    }
  ]
})

new Vue({

  router,

  render: h => h('frame', [h(Master)])

}).$start()
```

### In Components

```html
<Button @tap="$router.go('drawer')" text="Go To Drawer"/>
```

```javascript

  methods: {
    goToPage(page) {
      this.$router.go(
        page,
        { name: 'john', surname: 'doe' }
      )
    }
  }
```