import Vue from 'nativescript-vue'
import VueDevtools from 'nativescript-vue-devtools'
import Router from './router'
import Master from './components/Master'
import Detail from './components/Detail'
import Drawer from './components/Drawer'

Vue.registerElement(
  'RadSideDrawer',
  () => require('nativescript-ui-sidedrawer').RadSideDrawer
)


if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

let router = new Router({
  routes: [
    { 
      name: 'home',
      component: Master
    },
    { 
      name: 'detail',
      component: Detail
    },
    { 
      name: 'drawer',
      component: Drawer
    }
  ]
})

new Vue({

  router,

  render: h => h('frame', [h(Master)])

}).$start()