export function install(Vue) {

  Vue.mixin({

    beforeCreate() {
      if (this.$options.router) {
        this._router = this.$options.router
        this._router.init(this)
      }
    }
  })

  Object.defineProperty(Vue.prototype, '$router', {
    get() { return this.$root._router }
  })

}
