import { install } from './install'
import { find } from 'lodash'
import Vue from 'nativescript-vue'

export default class Router {

  constructor(options = {}) {

    if (!options.routes) {
      let message = 'ROUTER: Missing required array of routes'
      console.log(message)
      throw message // for some reason the message is not logged to the terminal when thrown.
    }

    this.routes = options.routes
    this.resetOptionsQueue()

    Vue.use(install)
  }

  init(app) {
    this.app = app
  }

  resetOptionsQueue() {
    this.optionsQueue = {}
  }

  get clear() {
    this.optionsQueue.clearHistory = true
    return this
  }

  go(name, props = {}, transitions = {}) {
    let route = find(this.routes, route => route.name === name)

    if (!route) {
      let message = 'ROUTER: 404 Not Found'
      console.error(message)
      return
    }

    this.app.$navigateTo(route.component, {
      props,
      transition: transitions.all,
      transitionIOS: transitions.ios,
      transitionAndroid: transitions.android,
      clearHistory: this.optionsQueue.clearHistory || false
    })

    this.resetOptionsQueue()
  }

  back() {
    this.app.$navigateBack()
  }

}
